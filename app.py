import json
import logging
import os

from flask import Flask
from redis import Redis

from scrapper.config import Config
from scrapper.scrapper import Scrapper

logging.basicConfig(level=logging.DEBUG)

app = Flask(__name__)
redis = Redis(host='redis', port=6379) 

@app.route('/example')
def example():
    redis.incr('hello_worlds')
    return 'This route has outputted a total of {} "hello worlds"'.format(int(redis.get('hello_worlds')))

@app.route('/')
def test():
    config = Config(os.path.join(*[
        os.path.dirname(os.path.realpath(__file__)),
        'configs',
        'scrap.json',
    ]))
    scraper = Scrapper(config)
    return json.dumps(scraper.scrape())

if __name__ == "__main__": 
    app.run(host='0.0.0.0', debug=True)
