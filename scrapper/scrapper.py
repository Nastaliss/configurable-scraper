

from selenium.common.exceptions import NoSuchElementException

from .config import Config
from .driver import Driver
from .interactions.interactions import Interactions
from .page import Page


class Scrapper(object):

    columns = []

    def __init__(self, config: Config):
        self.driver = Driver(config.url)
        self.container_xpath = config.targets_container
        self.interactions = Interactions(config.interactions, self.driver)
        self.columns_info = config.columns_info

    def scrape(self):
        self.interactions.execute('initial')

        paginated_data = []
        while True:
            self.interactions.execute('before_each_page')
            paginated_data.append(self.scrape_page())
            try:
                self.interactions.execute('after_each_page')
            except NoSuchElementException:
                break

        self.interactions.execute('final')
        return self.aggregate(paginated_data)

    def scrape_page(self):
        page = Page(self.driver, self.container_xpath)
        return page.scrape(self.columns_info)

    def aggregate(self, paginated_data):
        aggregated_data = {}
        for page_columns in paginated_data:
            for column in page_columns:    
                if not column['name'] in aggregated_data:
                    aggregated_data[column['name']] = []
                aggregated_data[column['name']].extend(column['values'])
            
        return aggregated_data
