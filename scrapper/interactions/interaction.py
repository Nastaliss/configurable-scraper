class Interaction:
    def __init__(self, interaction_config, driver):
        self.action_type = interaction_config['type']
        self.action_location = interaction_config['target']
        self.driver = driver

    def execute(self):
        if self.action_type == 'click':
            element = self.driver.find_element_by_xpath(self.action_location)
            self.driver.execute_script("arguments[0].click();", element)
