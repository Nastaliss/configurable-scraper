from .interaction import Interaction


class Interactions:
    def __init__(self, interactions_config, driver):
        self.initial = self.generate_interactions(interactions_config['initial'], driver)
        self.before_each_page = self.generate_interactions(interactions_config['beforeEachPage'], driver)
        self.after_each_page = self.generate_interactions(interactions_config['afterEachPage'], driver)
        self.final = self.generate_interactions(interactions_config['final'], driver)

    def generate_interactions(self, configs, driver):
        if len(configs) == 0:
            return []
        return [Interaction(config, driver) for config in configs]

    def execute(self, action_set):
        for action in self.__dict__[action_set]:
            action.execute() 
