import json
import os


class Config(object):

    def __init__(self, config_path: str):
        self.config_path = config_path
        self.parse_config()
        self.format_config()

    def parse_config(self):
        with open(self.config_path, 'r') as config:
            self.config = json.load(config) 

    def format_config(self):
        self.url = self.config['url']
        self.targets_container = self.config['targetsContainer']
        self.columns_info = self.config['columns']
        self.interactions = self.config['interactions']
