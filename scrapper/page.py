class Page: 

    def __init__(self, driver, container_xpath):
        self.container = driver.find_elements_by_xpath(container_xpath)
        self.driver = driver

    def scrape(self, columns_info):
        return self.generate_columns(columns_info)
        

    def generate_columns(self, columns_info):
        columns = []
        for column_info in columns_info.values():
            columns.append({
                "name": column_info['key'],
                "values": self.get_values(column_info)
                })
        return columns

    def get_values(self, column):
        values = []
        for sub_container in self.container:
            element = sub_container.find_element_by_xpath(column["target"])
            values.append(self.format_property(
                column['type'],
                element.get_attribute(column["property"])
                )) 
        return values

    def format_property(self, format_type, value ):
        if format_type == 'string':
            return str(value)
        if format_type == 'integer':
            return int(value)
        if format_type == 'float':
            return float(value)
