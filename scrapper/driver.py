from selenium import webdriver
from selenium.webdriver.chrome.options import Options


class Driver(webdriver.Chrome):

    def __init__(self, url):
        super().__init__(chrome_options=self.getOptions())
        self.get(url)
    
    @staticmethod
    def getOptions():
        chrome_options = Options()
        chrome_options.add_argument("--headless")
        chrome_options.add_argument("--no-sandbox")
        chrome_options.add_argument("window-size=1024,768")
        return chrome_options
