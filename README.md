# Configurable scraper

*This project was created using the [Python > Flask > Redis boilerplate](https://gitlab.com/simpleboilerplates/python/flask/redis?nav_source=navbar)*

## Presentation

This project was created to easily scrape any website without writing a whole lot of code.

In theory you should be able to get the data you want by only editing the **scrap.json** file located in the /configs/ folder

This is still a work in progress, any feedback would be appreciated